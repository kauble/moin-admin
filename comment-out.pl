#!/usr/bin/perl

# This script comments out lines in a first file based on patterns in
# a second file

# It's written in Perl because there's no way to pipe to sed -i

use warnings;
no warnings 'uninitialized';
use strict;

# Pop the parameters from the arguments, define script globals
my $list = pop @ARGV;
my $target = pop @ARGV;
my %patterns;

# Now open the pattern files using the given handles
open my SCRIPT, $target or die $!;
open my LIST, $patterns or die $!;
open my NEW, ">", $target.tmp or die $!;

# Load the list of patterns to be retained into a hash array
while (<LIST>) { $patterns = {$_}++; }
# Now begin scanning SCRIPT
while (<SCRIPT>) {

    my $line = $_;
    # Begin scanning $patterns for a possible match
    while ( ($hash, $dummy) = each %patterns) {

        # If a match, ensure $line is commented out, exit hash loop
        if ($line =~ m;$hash;) {
            $line =~ s;(# ok: )?(.*);# ok: $2;;
            last;
        }
    }

    # Print the possibly modified line to NEW
    print NEW $line;
}

# Close all file handles
close SCRIPT; close LIST; close NEW;
# Clobber the old version of the script
system("mv", "$target.tmp", "$target");
