#!/usr/bin/perl

# This script splits up MoinMoin event logs into chunks, each one
# spanning the given number of months

# It's written in Perl to allow for date processing and scanning the
# files much faster. As a result, it uses several backticks, maybe
# not the best idea, but simpler than the other options

# I've tried to avoid messing with CPAN, and other log processing is
# kept in the bash script to keep things simple

# To allow for multiple log files, the script returns the cutoff UTC
# of the preceding file

use warnings;
no warnings 'uninitialized';
use strict;

# Pop common parameters from script arguments, declare script globals
my $file = pop @ARGV;
my $interval = pop @ARGV;
my ($current, $lastline, $lastutc, $enddate, $stoputc);

# If "current" flag is set, stop the last file from becoming a rump
if ($ARGV[0] eq '-c') {

    # Drop the flag from the argument array
    $current = shift @ARGV;

    # Calculate the month of the last entry
    $lastline = `tail -n 1 $file`;
    $lastutc = substr $lastline, 0, 10;
    $enddate = `date -d "\@$lastutc" +%Y-%m-01`;

    # Then guarantee between 1 & 2 full intervals remain in the log
    $stoputc = `date -d "$enddate $interval ago" +%s`;
}

# Should be either a given cutoff UTC or undefined (perl handles as 0)
my $cututc = pop @ARGV;

# Open the logfile to read from
open BIGLOG, $file or die $!;

# If a previous cutoff UTC was passed, open the proper file
my ($tmpdate, $cutdate);
if ($cututc != 0) {
    $tmpdate = `date -d "\@$cututc" +%Y-%m-01`;
    $cutdate = `date -d "$tmpdate $interval yesterday" +%Y%m%d`;

    # Creates weird files with \n in the name otherwise
    chomp($cutdate);
    open OUTFILE, ">>", "wiki-log-$cutdate" or die $!;
}

# Start reading lines from the log, flag prevents redundant read/write
my $laterchunk = 0;
while (<BIGLOG>) {

    # Pull the date from the top line
    my $toputc = substr $_, 0, 10;

    # If a line is past the cutoff, switch to a new cutoff and file
    if ($toputc >= $cututc) {

        $tmpdate = `date -d "\@$toputc" +%Y-%m-01`;
        $cutdate = `date -d "$tmpdate $interval yesterday" +%Y%m%d`;
        chomp($cutdate);
        $cututc = `date -d "$tmpdate $interval" +%s`;

        # Break from the loop if the stop UTC will be passed
        if (($current eq '-c') && ($cututc > $stoputc)) { last; }

        # We know here the log will be split at least once, set flag
        $laterchunk = 1;
        # Open a file with the new name
        open OUTFILE, ">>", "wiki-log-$cutdate" or die $!;
    }

    # Write the current line to the appropriate file
    print OUTFILE $_;
}

# Perl preserves file position on breaking from a loop
# Move any leftover lines to a new version of the original logfile
if (($current eq '-c') && ($laterchunk)) {
    open OUTFILE, ">", "$file.tmp" or die $!;
    while (<BIGLOG>) { print OUTFILE $_; }
}

# Close both files just in case ...
close BIGLOG; close OUTFILE;
# ... clobber the old log if necessary ...
if -e $file.tmp { system("mv", "$file.tmp", "$file"); }
# ... and return $cututc for piping
print $cututc;
