# This Python program is for moving data from an HTML-style table to a
# moinmoin-style one.

# Usage: python mointabulate.py html_file moinmoin_file

# It doesn't attempt to parse a full HTML file, but goes straight for
# specific elements. As a result, it may help to just cut out the
# surrounding html from the files. The script will also take care of
# line-breaks, but embedded tags should probably just be handled by
# replace in a text editor.

# Before running, the output file should have at least a first line,
# consisting of a moinmoin-table header row with the desired header
# names. The script will interactively present the html headers it comes
# across, asking if they should be mapped to the wikitable or ignored.

# Although it could be extended to shuffle around columns and check for
# existing columns, I'm going to leave that to the user for now. It also
# uses easy-to-type, single letters for command codes. If you're using a
# lot of moinheaders you'll have to modify it so commands & row codes
# don't overlap

# For using 'end=' in print statements, pre v.3
from __future__ import print_function
# Regexps, some user interaction, and overwriting the Moin file
import re, os, shutil, tempfile

def run(infile, outfile):
    """Check command line arguments and call subprocedures."""

    try:
        html = open(infile)
    except IOError:
        print('The given HTML file could not be found.')

    try:
        wiki = open(outfile)
    except IOError:
        print('The given file could not be found or written to.')

    # Read & store moinmoin & html headers in lists
    moinheaders = scanmoin(wiki)
    htmlheaders = scanhtml(html, '<th>')

    # Run dialog with user, setup column mapping, and write data
    mapping = setupdict(moinheaders, htmlheaders)
    tmpfile = writedata(html, wiki, mapping, len(moinheaders))

    # Handle files and exit program
    html.close()
    wiki.close()
    # Should overwrite original version of moinmoin file
    shutil.move(tmpfile, outfile)
    return

# Some tweaks could possibly merge this with scanhtml
def scanmoin(wiki):
    """Scan the wiki table header & return the list of fields"""

    # This should leave the wiki file position at the 2nd line
    tmpheader = wiki.readline()
    tmplist = re.findall('\|[^|]+\|',tmpheader)

    # Pretty-print the moinmoin headers, index needed for mutability
    for i in range(len(tmplist)):
        tmplist[i] = re.sub('''[|'"*\t\n\r\f\v]''', ' ', tmplist[i])
        tmplist[i] = re.sub('  +', ' ', tmplist[i])
        tmplist[i] = re.sub('^\s+|\s+$', '', tmplist[i])

    # Reset the buffer to the beginning of the file just in case
    wiki.seek(0)
    return tmplist

def scanhtml(html, tag):
    """Scan the html for the tag & return the list of elements"""

    # Declare tmplist outside while block to be safe
    tmplist = []
    # Move to the first instance of the given tag
    found = findnexttag(html, tag)
    if (not found):
        print('Your HTML file has no ' + tag + ' elements')
        return

    # Read off elements and use contents for dictionary keys
    while (found):
        tmplist.append(scanelement(html, tag))
        found = findnexttag(html, tag)

    # Pretty-print the html elements
    for i in range(len(tmplist)):
        tmplist[i] = re.sub('[\t\n\r\f\v]', ' ', tmplist[i])
        tmplist[i] = re.sub('  +', ' ', tmplist[i])
        tmplist[i] = re.sub('^\s+|\s+$', '', tmplist[i])

    html.seek(0)
    return tmplist

def findnexttag(scanfile, tag):
    """Move to next occurrence of tag in either file"""

    # First line is a special case, may be multiple tags on that line
    firstline = True
    line = 'start'

    # To handle html attributes, I'll use findnexttag recursively
    if (tag[0] == '<'):
        target = tag[:-1]
        tagclose = '>'
    else:
        target = tag

    # Can't use "for," moving the buffer within a line confuses it
    while (line != ''):
        # Record the starting point and load a new line
        startpoint = scanfile.tell()
        line = scanfile.readline()

        # If the first line, we may have to skip earlier tags
        if (firstline):
            # Slice and scan the end of the line
            endpoint = scanfile.tell()
            unscanned = endpoint - startpoint
            lineend = line[-unscanned:]
            tagindex = lineend.find(target)
            # Switch off firstline flag
            firstline = False
        else:
            tagindex = line.find(target)

        # Occurs whenever the tag is found
        if (tagindex > -1):
            # Move file position to just past tag's close
            scanfile.seek(startpoint + tagindex + len(target))
            # AHHHH! Recursion with constant depth of 1 !
            if (target != '>'):
                findnexttag(scanfile, tagclose)
            return True

    # Return False if the tag isn't found
    return False

def scanelement(html, tag):
    """Read off a single html element"""

    # Create the closing tag from the given one, set len(closingtag)
    closingtag = tag[0] + '/' + tag[1:]
    closelen = len(closingtag)
    # joinlist is used to gather characters to join into a string
    joinlist = []

    # Read in single characters until the closingtag is reached
    # I've currently set the element limit to 4 kB
    for i in range(4096):
        joinlist.append(html.read(1))
        # Use list and join to allow for string concatenation
        tmpstring = ''.join(joinlist)
        # Chop off end and look for closingtag
        if (tmpstring[-closelen:] == closingtag):
            # Return trimmed line
            return tmpstring[:-closelen]

    # If the element exceeds the limit, print error to stdout
    print('You have a really massive element\n' +
            'or you may be missing a ' + closingtag + ' somewhere')
    return 'Check the HTML for this element'

def setupdict(moinheaders, htmlheaders):
    """Call dialog and setup html<->moin dictionary based on input"""

    # Initialize the dictionary and input formats at the proper scope
    mapping = {}
    mappattern = re.compile('([A-Za-z])\s*([0-9]+)?\s*$')
    # A flag for the loop that refreshes the display
    changing = True

    # Unchanging dimensions such as array lengths and column padding
    htmllen = len(htmlheaders)
    moinlen = len(moinheaders)
    moinpad = len(max(moinheaders, key=len)) + 3

    # Use dictionary (sortable, mutable) for tracking unmapped numbers
    freehtml = {i: None for i in range(htmllen)}

    while (changing):
        drawdisplay(moinheaders, htmlheaders, freehtml,
                                      mapping, moinpad)
        changing = handleinput(mapping, freehtml, mappattern,
                                            htmllen, moinlen)

    # If the script completes, return the dictionary for writing
    return mapping

def drawdisplay(moinheaders, htmlheaders, freehtml, mapping, moinpad):
    """Display instructions and the current mapping"""

    # Clear the screen to refresh
    if (os.name == 'nt'):
        os.system('cls')
    else:
        os.system('clear')

    # Draw up instructions
    print("To map fields, type the moinmoin field's letter code,\n"
        + "followed by the HTML field's number, then <enter>.\n"
        + "Typing a mapped field's letter will undo the selection,\n"
        + "'Y' will prompt you to finish, and 'X' will exit\n"
        + "the script without saving.\n")

    # Use a copy of freehtml to keep track of printing per refresh
    printed = dict(freehtml)
    # Used for leftover htmlheaders at the end, and as shorthand
    moinlen = len(moinheaders)
    # Print off headers for mapping, keep all moinheaders together
    for field in range(moinlen):

        # Determine letter code for moinmoin record: A-Z
        charlabel = unichr(field + 65)
        # Load first moinheader and determine the padding
        tmpmoin = moinheaders[field]
        padding = moinpad - len(tmpmoin)

        # Print the moinheader, check if mapped, then pad appropriately
        print(charlabel + '. ' + tmpmoin, end=' ')
        if (field in mapping):
            # Show link with '=', print allocated HTML header
            print('=' * padding, end=' ')
            print(htmlheaders[mapping[field]])
        elif (printed != {}):
            # Show blank spaces between unallocated HTML headers
            print(' ' * padding, end=' ')

            # Select 1st unmapped htmlheader, print, and cross it off
            nextheader = sorted(printed.keys())[0]
            print(str(nextheader) + '. ' + htmlheaders[nextheader])
            del printed[nextheader]
        else:
            # If there are no more unmapped htmlheaders, end line
            print()

    # Now print remaining unpaired entries, leaving break for prompt
    for leftover in printed:
        print((' ' * (moinpad + 5)) + str(moinlen) + '. '
                                    + printed[leftover])
        moinlen += 1
    print()
    return

def handleinput(mapping, freehtml, mappattern, htmllen, moinlen):
    """Prompt the user for input and process the command"""

    # Prompt user in a loop in case of bad input or canceled saves
    while (True):
        userinput = raw_input("Please enter your command: ")
        # Exit loop and program on quit command
        if (userinput == 'X' or userinput == 'x'):
            print('Exiting the script, maybe next time?')
            sys.exit(0)

        # For genuine saves, return a flag to break from both loops
        elif (userinput == 'Y' or userinput == 'y'):
            confirm = raw_input("Are you happy with your choices?\n"
                + "Enter 'Y' to confirm, all else cancels: ")
            if (confirm == 'Y' or confirm == 'y'):
                return False
            else:
                continue

        # Check that user input matches the pattern
        inputcode = mappattern.match(userinput)
        if (not inputcode):
            print('Input not valid. Mappings use a "letter #" format')
            continue

        # Check that the letter code is within proper bounds
        moinnum = ord(inputcode.group(1).upper()) - 65
        if (moinnum >= moinlen or moinnum < 0):
            print ('The letter code you entered is out of range')
            continue

        # If an undo, undo choice and return to dialog loop
        if (inputcode.group(2) == None):
            if (moinnum in mapping):
                # Add back as freehtml key and remove from mapping
                freehtml[mapping[moinnum]] = None
                del mapping[moinnum]
                return True
            else:
                print('That letter is not currently mapped')
                continue

        # For adds, check for range and if it is already mapped
        else:
            htmlnum = int(inputcode.group(2))
            if (htmlnum >= htmllen or htmlnum < 0):
                print('The number you selected is out of range')
                continue
            elif (moinnum in mapping):
                print('The letter you selected is currently occupied')
                continue
            elif (htmlnum not in freehtml):
                print('The number you selected is already mapped')
                continue
            else:
                # Add number to mapping and remove from freehtml
                mapping[moinnum] = htmlnum
                del freehtml[htmlnum]
                return True

def writedata(html, wiki, mapping, moinlen):
    """Write html table data to moinmoin table"""

    # Pulls out all table rows (and scrubs funky whitespace)
    htmlrows = scanhtml(html, '<tr>')
    # Counter is for if existing moinmoin columns run out first
    moincount = 0
    # Create temporary file for inserting modified lines
    tmphandle = tempfile.mkstemp()
    try:
        tmpbuffer = open(tmphandle[1], 'w')
    except IOError:
        print('There was an error with the temporary file.')

    # First HTML row, moinmoin line are headers, should be in sync
    firstline = True
    for line, row in zip(wiki, htmlrows):
        moincount += 1
        if (firstline):
            firstline = False
            tmpbuffer.write(line)
            continue

        # Retrieve <td> elements from row (non-greedy stars)
        htmlcells = re.findall('<td.*?>.*?</td>', row)
        # Skip if the HTML row is degenerate (no <td> tags)
        if (htmlcells == []):
            continue
        # Scrub <td> and </td> tags from each element
        for i in range(len(htmlcells)):
            htmlcells[i] = re.sub('(<td.*?> ?)|( ?</td>)', '',
                                                 htmlcells[i])

        # Use an array to store line changes
        joinlist = []
        # Use for marking existing column starts in a line
        colstart = line.find('||') + 2
        # Insert html entries into columns, left to right
        for col in range(moinlen):

            # Write the left pipes for the column regardless
            joinlist.append('|| ')
            # Copy over existing columns if nothing is mapped
            if ('||' in line[colstart:]):
                colend = line.find('||', colstart)
                if (col not in mapping):
                    joinlist.append(line[colstart:colend])
                colstart = colend + 2

            # If the column is mapped and has an entry in the HTML row
            if ((col in mapping) and (mapping[col] < len(htmlcells))):
                joinlist.append(htmlcells[mapping[col]] + ' ')

        # Add closing pipes, join list for modified line, and print
        joinlist.append('||\n')
        tmpstring = ''.join(joinlist)
        tmpbuffer.write(tmpstring)

    # Exit function if all HTML written, print new rows otherwise
    if (moincount < len(htmlrows)):
        for row in htmlrows[moincount:]:
            # Almost exact same as above, definitely refactorable
            htmlcells = re.findall('<td.*?>.*?</td>', row)
            if (htmlcells == []):
                continue
            for i in range(len(htmlcells)):
                htmlcells[i] = re.sub('(<td.*?> ?)|( ?</td>)', '',
                                                     htmlcells[i])

            joinlist = []
            for col in range(moinlen):
                joinlist.append('|| ')
                if ((col in mapping) and
                    (mapping[col] < len(htmlcells))):
                    joinlist.append(htmlcells[mapping[col]] + ' ')
            joinlist.append('||\n')
            tmpstring = ''.join(joinlist)
            tmpbuffer.write(tmpstring)

    # Should be finished now, close tmpfile and return path string
    tmpbuffer.close()
    return tmphandle[1]

if __name__ == "__main__":
    import sys
    run(str(sys.argv[1]), str(sys.argv[2]))
