#!/bin/sh
# This script will run a one-time clean of the WineWiki on Moin v1.5
# Remember this script assumes being run from wiki's parent dir

# *** Clean out the cache and pages ***
# Use the `moin` command to clear out the cache, check flags are correct
# For v1.5.7, "config/" is in "data/", not so in later versions
moin --config-dir=wiki/data/config --wiki-url=http://wiki.winehq.org/ \
maint cleancache

# Now use the cleanpage command to clear out the pages
moin --config-dir=wiki/data/config --wiki-url=http://wiki.winehq.org/ \
maint cleanpage > cleanpage.sh

# There should probably be a date-based grace period though
# First extract absolute paths from cleanpage and recent files
cat cleanpage.sh | sed "s;[^/]*\(/.*/\)[^/]*;\1;" > paths.tmp
find wiki/data/pages -mtime -365 | sed "s;\(.*\);$PWD/\1;" > recent.tmp
# Then isolate the common files
comm -12 paths.tmp recent.tmp > retain.tmp
# And comment out those lines in cleanpage (using a perl script)
perl comment-out.pl cleanpage.sh retain.tmp

# The cleanpage script won't touch EditorBackups, perhaps it should
# NOTE: Recent versions no longer make these pages, they won't be an
#     issue after the migration
# Since the patterns contain '/',':' & '#', use ';' for delimiters
sed -i "s;\(# ok: \)\('.*MoinEditorBackup'\);mv \2 trash # trash;" \
cleanpage.sh

# After checking the script, run it, then wipe and remake the folders
./cleanpage.sh
rm -rf wiki/data/trash
rm -rf wiki/data/deleted
rm cleanpage.sh
rm *.tmp
mkdir wiki/data/trash
mkdir wiki/data/deleted

# This will CHECK for any duplicate accounts
# (use "--save" to write changes)
moin --config-dir=wiki/data/config --wiki-url=http://wiki.winehq.org/ \
account check --usersunique
moin --config-dir=wiki/data/config --wiki-url=http://wiki.winehq.org/ \
account check --emailsunique

# But how to scan by dates? Use the mod times of the trail files,
# which are still present in v1.5.7

# After that, looks like finding "last-visit" data won't be guaranteed
# until at least after v2

USERDIR="wiki/data/user"
# This first step hunts down users with trails older than 1 year
find $USERDIR -mtime +365 -iname "*.trail" | sed \
"s;\(.*\)\.trail;\1\*;" > abandoned.tmp
# Then deletes them from the directory
rm $(cat abandoned.tmp)

# Just to be sure, check for users without trails, and vice versa
find $USERDIR \! -iname "*.trail" \! -iname "*.bookmark" | sed \
"s;\(.*\);\1\*;" > users.tmp
find $USERDIR -iname "*.trail" | sed \
"s;\(.*\)\.trail;\1\*;" \ > trails.tmp
comm -3 users.tmp trails.tmp > missing.tmp
# ... and abandoned bookmarks
find $USERDIR -iname "*.bookmark" | sed \
"s;\(.*\)\.bookmark;\1\*;" > marks.tmp
comm -13 users.tmp marks.tmp >> missing.tmp

# If everything looks good, clear out the cruft
rm $(cat missing.tmp)
rm *.tmp

# *** Redo the logs (only needed at most once every migration) ***
# First, merge the two edit-logs as they're actually quite compact
# Clip off the unique ending of edit-log and store temporarily
comm -13 edit-log.1 edit-log >> log-end.tmp
# Append the unique portion of edit-log to edit-log.1
cat log-end.tmp >> edit-log.1
# and clobber the incomplete log and temporary file
mv edit-log.1 edit-log
rm log-end.tmp

# Now to take care of the event-logs
# Scan every line in the remaining logs using a tiny perl script
TMPUTC=$(perl split-logs.pl "3 months" event-log.1)
perl split-logs.pl -c $TMPUTC "3 months" event-log

# Now event-log.1 should be safe to delete
rm event-log.1
